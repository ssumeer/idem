aiofiles>=0.7.0
acct>=8.3.2,<9.0.0
dict-toolbox>=2.2.0,<3.0.0
jinja2
jmespath
pop>=22.0.1
pop-evbus>=6.1.0,<7.0.0
pop-config>=10.2.1,<11.0.0
pop-loop>=1.0.4,<2.0.0
pop-serial>=1.1.0,<2.0.0
pop-tree>=9.2.1,<10.0.0
PyYaml>=6.0
toml
tqdm
rend>=6.5.0,<7.0.0
colorama
wheel
