dep-1:
  test.unique_op:
    - require:
      - test: nop-1

dep-2:
  test.unique_op:
    - require:
      - test: nop-2

dep-3:
  test.unique_op:
    - require:
      - test: nop-3

nop-1:
  test.nop:
    - require:
      - test: nop-2

nop-2:
  test.nop:
    - require:
      - test: nop-3

nop-3:
  test.nop
