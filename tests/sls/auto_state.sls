test-autostate-create:
  tests.auto.present:
    - name: foo
    - param: 1

test-autostate-present:
  tests.auto.present:
    - name: foo
    - param: 1

test-autostate-update:
  tests.auto.present:
    - name: foo
    - param: 2

test-autostate-delete:
  tests.auto.absent:
    - name: foo

test-autostate-absent:
  tests.auto.absent:
    - name: foo
