def test_missing_required_params(idem_cli, tests_dir):
    # Test that function is not called if required param/s are missing
    ret = idem_cli("state", tests_dir / "sls" / "missing_params.sls")
    assert ret.result is False
    print(ret.stdout)
    assert (
        "states.test.required is missing required argument(s): required_param"
        in ret.stdout
    )


def test_invalid_bool_params(idem_cli, tests_dir):
    # Test that function is not called if a boolean type param is a non boolean value
    ret = idem_cli("state", tests_dir / "sls" / "invalid_param.sls")
    assert ret.result is False
    assert (
        "states.test.required is expecting a boolean value for 'bool_param' but got 'not_a_boolean_value'"
        in ret.stdout
    )
