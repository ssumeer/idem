import contextlib
import os


def test_normalize(hub):
    ret = hub.idem.sls_source.init.normalize("foo.bar/baz.bop.sls")
    assert ret == "foo\bbar.baz\bbop"


def test_normalize_already_normalized(hub):
    ret = hub.idem.sls_source.init.normalize("foo\bbar.baz\bbop")
    assert ret == "foo\bbar.baz\bbop"


def test_de_normalize(hub):
    ret = hub.idem.sls_source.init.de_normalize("foo\bbar.baz\bbop")
    assert ret == "foo.bar/baz.bop"


def test_de_normalize_already_denormalized(hub):
    ret = hub.idem.sls_source.init.de_normalize("foo.bar/baz.bop")
    assert ret == "foo.bar/baz.bop"


def test_de_normalize_relative(hub):
    ret = hub.idem.sls_source.init.de_normalize("...foo.ref")
    assert ret == "../../../foo/ref"


def test_normalize_relative(hub):
    ret = hub.idem.sls_source.init.normalize("../../../foo/ref")
    assert ret == "...foo.ref"


def test_idem_run_with_config_dot_dirs(idem_cli, tests_dir):
    tree = tests_dir / "sls" / "config.with.dots"
    config_file = (tree / "idem.cfg").relative_to(tests_dir.parent)

    ret = idem_cli(
        "state",
        f"--config={config_file}",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_config_relative_dot_dirs(idem_cli, tests_dir):
    tree = tests_dir / "sls" / "config.with.dots"
    config_file = (tree / "idem.cfg").relative_to(tests_dir.parent)

    ret = idem_cli(
        "state",
        "state.dotted/relative.sls",
        f"--config={config_file}",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    # This is not valid syntax, unless we also want to try ./*/*/*/*/*/<ref> as well as ../../../../../../<ref>
    assert ret.result is False


def test_idem_run_with_config_implied_dot_dirs(idem_cli, tests_dir):
    tree = tests_dir / "sls" / "config.with.dots"
    config_file = (tree / "idem.cfg").relative_to(tests_dir.parent)

    ret = idem_cli(
        "state",
        "state.dotted/implied.sls",
        f"--config={config_file}",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout


@contextlib.contextmanager
def set_cwd(path):
    # Store the previous working directory
    cwd = os.getcwd()
    try:
        # Change working directory to the given path
        os.chdir(path)
        yield
    finally:
        # No matter what happens, restore the previous working directory
        os.chdir(cwd)


def test_idem_run_with_config_deep_nested_file_paths(idem_cli, tests_dir):
    deep_nested_wd = (
        tests_dir / "sls" / "deep_nested" / "nest1" / "nest2" / "nest3" / "nest4" / "wd"
    )
    with set_cwd(deep_nested_wd):
        ret = idem_cli(
            "state",
            f"--config=file_path.cfg",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_config_deep_nested_refs(idem_cli, tests_dir):
    deep_nested_wd = (
        tests_dir / "sls" / "deep_nested" / "nest1" / "nest2" / "nest3" / "nest4" / "wd"
    )
    with set_cwd(deep_nested_wd):
        ret = idem_cli(
            "state",
            f"--config=refs.cfg",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_config_deep_nested_no_backpedal_refs(idem_cli, tests_dir):
    deep_nested_wd = (
        tests_dir / "sls" / "deep_nested" / "nest1" / "nest2" / "nest3" / "nest4" / "wd"
    )
    with set_cwd(deep_nested_wd):
        ret = idem_cli(
            "state",
            f"--config=no_backpedal_refs.cfg",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_config_deep_nested_no_backpedal_file_paths(idem_cli, tests_dir):
    deep_nested_wd = (
        tests_dir / "sls" / "deep_nested" / "nest1" / "nest2" / "nest3" / "nest4" / "wd"
    )
    with set_cwd(deep_nested_wd):
        ret = idem_cli(
            "state",
            f"--config=no_backpedal_file_paths.cfg",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_top_level_file(idem_cli, tests_dir):
    wd = tests_dir / "sls" / "with.dots"
    with set_cwd(wd):
        ret = idem_cli(
            "state",
            f"--config=file_path.cfg",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_top_level_file_no_config(idem_cli, tests_dir):
    wd = tests_dir / "sls" / "with.dots"
    with set_cwd(wd):
        ret = idem_cli(
            "state",
            "file.name.with.dots.sls",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_top_level_ref(idem_cli, tests_dir):
    wd = tests_dir / "sls" / "with.dots"
    with set_cwd(wd):
        ret = idem_cli(
            "state",
            f"--config=null_character_ref.cfg",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_top_level_ref_no_config(idem_cli, tests_dir):
    wd = tests_dir / "sls" / "with.dots"
    with set_cwd(wd):
        ret = idem_cli(
            "state",
            "file\bname\bwith\bdots",
            "--esm-plugin=null",
            env={"ACCT_FILE": "", "ACCT_KEY": ""},
        )
        assert ret.result is True, ret.stderr or ret.stdout
        assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_top_level_ref_tree(idem_cli, tests_dir):
    wd = tests_dir / "sls" / "with.dots"
    ret = idem_cli(
        "state",
        f"--tree={wd}",
        "file\bname\bwith\bdots",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout


def test_idem_run_with_top_level_file_tree(idem_cli, tests_dir):
    wd = tests_dir / "sls" / "with.dots"
    ret = idem_cli(
        "state",
        f"--tree={wd}",
        "file.name.with.dots.sls",
        "--esm-plugin=null",
        env={"ACCT_FILE": "", "ACCT_KEY": ""},
    )
    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stderr or ret.stdout
