def test_sls_shell(tree, idem_cli):
    ret = idem_cli(
        "state",
        "simple",
        "--output=state",
        "--sls-sources",
        f"file://{tree}",
        check=True,
    ).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_sls_implied_source(code_dir, idem_cli):
    sls = code_dir.joinpath("tests", "sls", "simple.sls")
    ret = idem_cli("state", "--output=state", sls, check=True).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_sls_tree(tree, idem_cli):
    ret = idem_cli(
        "state", "simple", "--output=state", "--tree", tree, check=True
    ).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_fail(tree, idem_cli):
    ret = idem_cli("state", "--output=state", "nonexistant.sls", tree, check=False)
    assert "Could not find SLS ref 'nonexistant.sls' in sources" in ret.stdout
