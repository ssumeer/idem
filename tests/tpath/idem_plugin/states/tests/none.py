"""
This plugin existing tests the signature of the resource contract
"""
__contracts__ = ["resource"]


def present(hub, ctx, name: str):
    ...


def absent(hub, ctx, name: str):
    ...


async def describe(hub, ctx):
    ...
